**Note: This project is a work in progress.  Source code incomplete.**

# Team Tracker #

This is a web application that enables users to track game scores for their favourite sports teams.

### Component Diagram ###

![picture](TeamTracker.jpg)

### Functional Summary ###

* Users may subscribe to daily email notifications to track the score for their preferred sports teams.
* Batch process runs to update sports scores in local database.  Batch process will send email notifications once daily.
* Sports scores sourced from Twitter.  Leagues supported: MLB, NBA, NFL, NHL.

### Technical Summary ###

* Front end: AngularJS
* Back end: Java REST services
* Data store: MongoDB

### Source Code ###

* TBD