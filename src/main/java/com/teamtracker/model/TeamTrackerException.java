package com.teamtracker.model;

public class TeamTrackerException extends Exception {

	private static final long serialVersionUID = 1L;

	public TeamTrackerException () {

    }

    public TeamTrackerException (String message) {
        super (message);
    }

    public TeamTrackerException (Throwable cause) {
        super (cause);
    }

    public TeamTrackerException (String message, Throwable cause) {
        super (message, cause);
    }
	
}
