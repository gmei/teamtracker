package com.teamtracker.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class GameScore {
	
	private String league;
	private List<TeamScore> teamScores;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/New_York")
	private Date gameDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = "America/New_York")
	private Date statusDate;
	
	public GameScore()
	{
		teamScores = new ArrayList<TeamScore>();
	}
	
	public String getLeague() {
		return league;
	}
	public void setLeague(String league) {
		this.league = league;
	}
	public List<TeamScore> getTeamScores() {
		return teamScores;
	}
	public void setTeamScores(List<TeamScore> teamScores) {
		this.teamScores = teamScores;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public Date getGameDate() {
		return gameDate;
	}
	public void setGameDate(Date gameDate) {
		this.gameDate = gameDate;
	}

	public void addTeamScore(TeamScore teamScore)
	{
		teamScores.add(teamScore);
	}
}
