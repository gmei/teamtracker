package com.teamtracker.model;

public class TeamScore {
	
	private String teamCode;
	private int points;
	
	public TeamScore()
	{
		// nothing here
	}
	
	public TeamScore(String teamCode, int points)
	{
		this.teamCode = teamCode;
		this.points = points;
	}
	
	public String getTeamCode() {
		return teamCode;
	}
	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
}
