package com.teamtracker.score;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.teamtracker.model.GameScore;
import com.teamtracker.model.TeamScore;
import com.teamtracker.model.TeamTrackerException;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

@Component
public class TwitterScoreFeed implements ScoreFeed {
	
	@Autowired
	private Environment environment;
	
	// number of games to retrieve
	private final int PAGE_SIZE = 64;
	
	public List<GameScore> getScores() throws TeamTrackerException
	{
		// game results
		List<GameScore> scores = new ArrayList<GameScore>();
		
		// find cut-off date for tweets
		Date cutOffDate = getCutoffDate();

		ResponseList<Status> tweets;
		try 
		{
			// get twitter configuration from application properties
			ConfigurationBuilder config = new ConfigurationBuilder();
			config.setOAuthConsumerKey(environment.getProperty("Twitter.ConsumerKey"));
			config.setOAuthConsumerSecret(environment.getProperty("Twitter.ConsumerSecret"));
			config.setOAuthAccessToken(environment.getProperty("Twitter.AccessToken"));
			config.setOAuthAccessTokenSecret(environment.getProperty("Twitter.AccessTokenSecret"));
			
			// retrieve twitter user's timeline
			Twitter twitter = new TwitterFactory(config.build()).getInstance();
			Paging paging = new Paging();
			paging.setCount(PAGE_SIZE);
			tweets = twitter.getHomeTimeline(paging);
		} 
		catch (TwitterException e) 
		{
			throw new TeamTrackerException(e.getErrorMessage(), e);
		}
		
		// iterate over tweets
		for (Status tweet : tweets) 
		{
			// validate tweet
			if (isValidScore(tweet))
			{
				try
				{
					// process tweet
					GameScore score = processTweet(tweet);
					
					// check if game is within last day
					if (score.getStatusDate().compareTo(cutOffDate) >= 0)
					{
						scores.add(score);
					}
				}
				catch (Exception e)
				{
					// ignore tweet that fails processing
				}
			}
		}
		
		return scores;
	}
	
	
	// process tweet to extract game result
	private static GameScore processTweet(Status tweet) throws Exception
	{
		GameScore score = new GameScore();
		
		// set league
		if (tweet.getUser().getScreenName().contains("MLB"))
		{
			score.setLeague("MLB");
		}
		else if (tweet.getUser().getScreenName().contains("NBA"))
		{
			score.setLeague("NBA");
		}
		else if (tweet.getUser().getScreenName().contains("NHL"))
		{
			score.setLeague("NHL");
		}
		else if (tweet.getUser().getScreenName().contains("NFL"))
		{
			score.setLeague("NFL");
		}
		else
		{
			throw new Exception("League not recognized for user: " + tweet.getUser().getScreenName());
		}
		
		// extract the score
		String[] tweetTokens = tweet.getText().split(" ");
		
		// team codes
		String teamCode1 = tweetTokens[tweetTokens.length - 2].substring(1);
		String teamCode2 = tweetTokens[tweetTokens.length - 1].substring(1);
		
		// point values
		int indexOfDash = Arrays.asList(tweetTokens).indexOf("-");
		int teamPoints1 = Integer.parseInt(tweetTokens[indexOfDash - 1]);
		int teamPoints2 = Integer.parseInt(tweetTokens[indexOfDash + 1]);
		score.addTeamScore(new TeamScore(teamCode1, teamPoints1));
		score.addTeamScore(new TeamScore(teamCode2, teamPoints2));
		
		// extract date
		score.setStatusDate(tweet.getCreatedAt());
		
		// determine game date
		// if tweet was sent in the morning, game was played previous day
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(tweet.getCreatedAt());
		if (calendar.get(Calendar.AM_PM) == Calendar.AM)
		{
			calendar.add(Calendar.DATE, -1);
		}
		score.setGameDate(calendar.getTime());
		
		return score;
	}
	
	
	// check if tweet is correctly formatted
	private static boolean isValidScore(Status tweet)
	{
		// perform regex check against the tweet
		String tweetText = tweet.getText();
		Pattern pattern = Pattern.compile("^The .*? won .*? over the .*? #.*? #.*?$");
		Matcher matcher = pattern.matcher(tweetText);
		
		return matcher.matches();
	}
	
	
	// calculate cutoff date for tweets
	private Date getCutoffDate()
	{
		Date cutOffDate = null;
		Calendar currentDate = Calendar.getInstance();
		
		// if query was sent in the morning, we want scores from previous day
		if (currentDate.get(Calendar.AM_PM) == Calendar.AM)
		{
			currentDate.add(Calendar.DATE, -1);
		}

		currentDate.set(Calendar.HOUR_OF_DAY, 12);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		cutOffDate = currentDate.getTime();
		
		return cutOffDate;
	}
	
}
