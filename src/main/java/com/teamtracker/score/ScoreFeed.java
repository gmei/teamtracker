package com.teamtracker.score;

import java.util.List;

import com.teamtracker.model.GameScore;
import com.teamtracker.model.TeamTrackerException;

public interface ScoreFeed {

	List<GameScore> getScores() throws TeamTrackerException;
}
