package com.teamtracker.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teamtracker.model.GameScore;
import com.teamtracker.score.ScoreFeed;
import com.teamtracker.score.TwitterScoreFeed;

@RestController
public class BatchController {
	
	@Autowired
	ScoreFeed scoreFeed;
	
	@RequestMapping(value="/ProcessScores", method=RequestMethod.GET)
    public List<GameScore> load()
    {
		List<GameScore> scores = null;

		try
		{
			scores = scoreFeed.getScores();
		}
		catch (Exception e)
		{
			//
		}
		
		return scores;
    }
	
	
}
