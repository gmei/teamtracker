package com.teamtracker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teamtracker.model.GameScore;
import com.teamtracker.score.ScoreFeed;

@RestController
public class ScoreController {
	
	@Autowired
	ScoreFeed scoreFeed;
	
	@RequestMapping(value="/GetScores", method=RequestMethod.GET)
    public List<GameScore> load()
    {
		List<GameScore> scores = null;

		try
		{
			scores = scoreFeed.getScores();
		}
		catch (Exception e)
		{
			//
		}
		
		return scores;
    }
	
	
}
